#!/usr/bin/env python
#
# -*- coding: utf-8
#
import os
from fastapi import FastAPI, Header, Body, File, UploadFile, Form
from starlette.responses import Response
from pydantic import BaseModel
from enum import Enum
from typing import List
from vaultservice import ServiceProcessor

version_info = (0, 1, 0)

web_version = lambda: '{0}.{1}.{2}'.format(*version_info)

sp = ServiceProcessor(backend_configs=os.environ.get('BACKEND_CONFIGS_FILE'),
                      project_template=os.environ.get('PROJECT_TEMPLATE_FILE'))

app = FastAPI(title='Harmony Vault Store REST API module',
              description='REST API version of the Vault Project Data Manager',
              version=web_version())


class ResponseModel(BaseModel):
    status: bool
    message: str

class ResponseStatusEnum(str, Enum):
    success: str = 'success'
    fail: str = 'fail'

class SecretUpdateMode(str, Enum):
    replace = 'replace'
    update = 'update'
    append = 'append'

class CertStoreFormat(str, Enum):
    auto = 'auto'
    plain = 'plain'
    base64 = 'base64'

class CertKind(str, Enum):
    ca_bundle = 'ca-bundle'
    crt = 'crt'
    key = 'key'
    pkcs12 = 'pkcs12'
    jks = 'jks'

class CertReadFormat(str, Enum):
    native = 'native'
    base64 = 'base64'
    json = 'json'
    passcode = 'passcode'

class CertWriteOptions(BaseModel):
    passcode: str = ''

class ProjectModel(BaseModel):
    project: str
    environ: str
    component: str
    version: str

class SecretModel(BaseModel):
    category: str
    alias: str

class ResponseProjectModel(ResponseModel):
    project: ProjectModel

class CertResponseModel(ResponseModel):
    fqdn: str
    kind: str

class CertReadResponseModel(CertResponseModel):
    data: dict

class ResponseProjectSecretModel(ResponseModel):
    project: ProjectModel
    secret: SecretModel

class ResponseReadProjectSecretModel(ResponseProjectSecretModel):
    data: dict = {}


@app.post('/v1/project/{project}/{environ}/{component}/{version}')
async def create_project(project, environ, component, version,
                         mode: SecretUpdateMode = SecretUpdateMode.replace,
                         x_vault_token: str = Header(None)):
    project = ProjectModel(project=project, environ=environ, component=component, version=version)
    status, message = sp.create_project(project_config=project.dict())
    return ResponseProjectModel(project=project, success=status, message=message)



@app.delete('/v1/project/{project}/{environ}/{component}/{version}')
async def delete_project(project, environ, component, version,
                         mode: SecretUpdateMode = SecretUpdateMode.replace,
                         x_vault_token: str = Header(None)):
    project = ProjectModel(project=project, environ=environ, component=component, version=version)
    status, message = sp.delete_project(project_config=project.dict())
    return ResponseProjectModel(details=project, success=status, message=message)



@app.post('/v1/project/{project}/{environ}/{component}/{version}/{category}/{alias:path}')
async def write_project_secret(project, environ, component, version, category, alias,
                               mode: SecretUpdateMode = SecretUpdateMode.replace,
                               data: dict = Body(...), x_vault_token: str = Header(None)):
        return action_on_project_secret(sp.write_projectsecret,
                                        project, environ, component, version,
                                        category, alias, mode,
                                        data, x_vault_token)



#@app.post('/v1/certificate/by-fqdn/{fqdn}/{kind}')
#async def write_certificate(fqdn, kind: CertKind,
#                            content_format: CertStoreFormat = CertStoreFormat.auto,
#                            x_vault_token: str = Header(None)):
#    pass


@app.post('/v1/certificate/by-fqdn/{fqdn}/{kind}')
async def write_certificate(*, fqdn, kind: CertKind,
                            file: UploadFile = File(...),
                            passcode: str = Form(...),
                            x_vault_token: str = Header(None)):
    content = await file.read()
    ok, content, message = sp.write_certificate(authtoken=x_vault_token, fqdn=fqdn,
                                                kind=kind, content=content,
                                                passcode=passcode)
    return CertResponseModel(fqdn=fqdn, kind=kind, status=ok, message=message)


@app.get('/v1/certificate/by-fqdn/{fqdn}/{kind}')
async def read_certificate(fqdn, kind: CertKind, format: CertReadFormat = CertReadFormat.native,
                           x_vault_token: str = Header(None)):
    ok, content, message = sp.read_certificate(authtoken=x_vault_token, fqdn=fqdn,
                                               kind=kind, format=format)
    if ok:
        if format in [CertReadFormat.native, CertReadFormat.base64, CertReadFormat.passcode]:
            #return Response(content=content, media_type='application/octet-stream')
            return Response(content)
        else:
            return CertReadResponseModel(fqdn=fqdn, kind=kind, status=ok,
                                         message=message, data=content)
    return CertReadResponseModel(fqdn=fqdn, kind=kind, status=ok, message=message)


@app.delete('/v1/certificate/by-fqdn/{fqdn}/{kind}')
async def delete_certificate(fqdn, kind: CertKind,
                             x_vault_token: str = Header(None)):
    ok, content, message = sp.delete_certificate(authtoken=x_vault_token, fqdn=fqdn,
                                                 kind=kind)
    return CertResponseModel(fqdn=fqdn, kind=kind, status=ok, message=message)



@app.get('/v1/certificate/by-project/{project}/{environ}/{component}/{version}/{alias}/{kind}')
async def read_project_certificate(project, environ, component, version, alias, kind,
                                   format: CertStoreFormat, x_vault_token: str = Header(None)):
    pass

@app.post('/v1/certificate/by-project/{project}/{environ}/{component}/{version}/{alias}/{kind}')
async def write_project_certificate(project, environ, component, version, alias, kind,
                                    format: CertStoreFormat, x_vault_token: str = Header(None)):
    pass

@app.delete('/v1/certificate/by-project/{project}/{environ}/{component}/{version}/{alias}/{kind}')
async def delete_project_certificate(project, environ, component, version, alias, kind,
                                     format: CertStoreFormat, x_vault_token: str = Header(None)):
    pass


@app.delete('/v1/project/{project}/{environ}/{component}/{version}/{category}/{alias:path}')
async def delete_project_secret(project, environ, component, version, category, alias,
                                mode: SecretUpdateMode = SecretUpdateMode.replace,
                                x_vault_token: str = Header(None)):
        return action_on_project_secret(sp.delete_projectsecret,
                                        project, environ, component, version,
                                        category, alias, mode,
                                        {}, x_vault_token)


@app.get('/v1/project/{project}/{environ}/{component}/{version}/{category}/{alias:path}')
async def read_project_secret(project, environ, component, version, category, alias,
                              mode: SecretUpdateMode = SecretUpdateMode.replace,
                              x_vault_token: str = Header(None)):
        return action_on_project_secret(sp.read_projectsecret,
                                        project, environ, component, version,
                                        category, alias, mode,
                                        {}, x_vault_token)


def action_on_project_secret(handler, project, environ, component, version, category, alias,
                             mode: SecretUpdateMode = SecretUpdateMode.replace,
                             data: dict = Body(...), x_vault_token: str = Header(None)):
    auth_token = x_vault_token
    project = ProjectModel(authtoken=auth_token, project=project,
                           environ=environ, component=component, version=version)
    secret = SecretModel(category=category, alias=alias)

    print("SEND DATA", data)

    status, message, output = handler(authtoken=auth_token, project=project.dict(),
                                    secret=secret.dict(), data=data)
    if handler == sp.read_projectsecret:
        return ResponseReadProjectSecretModel(project=project, secret=secret, success=status,
                                              message=message, data=output)
    else:
        return ResponseProjectSecretModel(project=project, secret=secret, success=status,
                                          message=message)

