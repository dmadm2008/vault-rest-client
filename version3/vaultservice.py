#!/usr/bin/env python
#
# -*- coding: utf-8
#

import os
import json
import yaml
import logging
import requests
import argparse
from base64 import encodebytes as encodebytes_2base64
from base64 import encodestring as encodestring_2base64
from base64 import b64decode, b64encode, decodestring
from json import loads as json_loads


def _(**kwargs):
    print(kwargs['msg'].format(**kwargs))


def validate_params(required=[], optional=[], params={}):
    passed_keys = list(params.keys())
    passed_unexpected, passed_required, passed_optional = [], [], []
    for k in passed_keys:
        if (k in required) and (params[k] not in ('', None)):
            passed_required.append(k)
        elif (k in optional):
            passed_optiona.append(k)
        else:
            passed_unexpected.append(k)
    assert(len(required) == len(passed_required)), \
        "Missed required params or their values: {0}".format(set(required) - set(passed_required))
    assert(passed_unexpected == []), \
        "Passed unexpected params: {0}".format(passed_unexpected)


def load_config(json_file=None, yaml_file=None):
    if not any([json_file, yaml_file]): #or not any([os.path.exists(json_file), os.path.exists(yaml_file)]):
        logging.error(_(msg="None of the provided parameters were given or provided not existed source(s): "
                            "json_file({json}), or yaml_file({yaml})", json=json_file, yaml=yaml_file))
        return
    else:
        filename = json_file if json_file else yaml_file
        loader = json.load if json_file else yaml.load
        with open(filename, 'r') as fd:
            try:
                buffer = loader(fd)
                logging.info(_(msg="Loaded configuration from file '{file}'", file=filename))
                return buffer
            except Exception as exc:
                logging.error(_(msg="Unable to load configuration from file '{file}': {error}",
                                file=filename, error=exc))


def render_config(json_config, params):

    def render_item(value):
        if isinstance(value, (str,)):
            return value.format(**params)
        elif isinstance(value, (list, tuple)):
            return [render_item(v) for v in value]
        elif isinstance(value, dict):
            return {k: render_item(v) for k, v in value.items()}
        else:
            raise Exception("This point should not be reached")
    return render_item(json_config)


class VaultEngine(object):
    def __init__(self, http_client, prefix):
        self.http = http_client
        self.engine_prefix = prefix
    def write(self, **kwargs):
        return
    def read(self, **kwargs):
        return
    def delete(self, **kwargs):
        return

class VaultSecret(VaultEngine):
    def __init__(self, http_client, prefix):
        """
        prefix - engine prefix, like /projects
        """
        super().__init__(http_client, prefix)

    def write(self, **kwargs):
        """
        Accepted parameters:
        - namespace_pattern
        - namespace_params (dict):
          - project
          - environ
          - component
          - version
          - namespace
        - payload (dict)
        """
        endpoint = '{0}/data{1}'.format(self.engine_prefix,
                                        (kwargs['namespace_pattern']).format(**kwargs['namespace_params']))
        return self.http.submit(endpoint=endpoint, good_http_codes=(200,),
                                payload=kwargs['payload'], method='PUT')

    def read(self, **kwargs):
        endpoint = '{0}/data{1}'.format(self.engine_prefix,
                                        (kwargs['namespace_pattern']).format(**kwargs['namespace_params']))
        return self.http.submit(endpoint=endpoint, good_http_codes=(200,), method='GET')

    def delete(self, **kwargs):
        endpoint = '{0}/metadata{1}'.format(self.engine_prefix,
                                        (kwargs['namespace_pattern']).format(**kwargs['namespace_params']))
        return self.http.submit(endpoint=endpoint, good_http_codes=(204,), method='DELETE')



class VaultSecret2(VaultEngine):
    """
    This version uses function submit2() for all backend operations
    which return a tuple of three (status, content, message)
    """
    def __init__(self, http_client, prefix):
        """
        prefix - engine prefix, like /projects
        """
        super().__init__(http_client, prefix)

    def write(self, **kwargs):
        """
        Accepted parameters:
        - namespace_pattern
        - namespace_params (dict):
          - project
          - environ
          - component
          - version
          - namespace
        - payload (dict)
        """
        endpoint = '{0}/data{1}'.format(self.engine_prefix,
                                        (kwargs['namespace_pattern']).format(**kwargs['namespace_params']))
        return self.http.submit2(endpoint=endpoint, good_http_codes=(200,),
                                payload=kwargs['payload'], method='PUT')

    def read(self, **kwargs):
        endpoint = '{0}/data{1}'.format(self.engine_prefix,
                                        (kwargs['namespace_pattern']).format(**kwargs['namespace_params']))
        return self.http.submit2(endpoint=endpoint, good_http_codes=(200,), method='GET')

    def delete(self, **kwargs):
        endpoint = '{0}/metadata{1}'.format(self.engine_prefix,
                                        (kwargs['namespace_pattern']).format(**kwargs['namespace_params']))
        return self.http.submit2(endpoint=endpoint, good_http_codes=(204,), method='DELETE')


class HttpClient(requests.Session):
    def __init__(self, url, headers={}):
        super().__init__()
        self.url = url
        self.headers.update(headers)

class BackendBase(HttpClient):
    def __init__(self, url, config, headers={}):
        super().__init__(url=url, headers=headers)
        self.headers.update({'Content-Type': 'application/json'})
        self.config = config

    @property
    def last_status_code(self):
        return self.__status_code

    def submit(self, **kwargs):
        good_codes = kwargs.pop('good_http_codes')
        endpoint = kwargs.pop('endpoint')
        url = '{0}{1}'.format(self.url, endpoint)
        payload = kwargs.pop('payload') if 'payload' in kwargs.keys() else {}
        response = self.request(url=url, json=payload, **kwargs)
        self.__is_ok = response.status_code in good_codes
        status_str = ('SUCCESS({status_code})' if self.__is_ok else 'FAIL({status_code})').format(status_code=response.status_code)
        logging.debug(_(msg='Request {method} on {endpoint} completed with {status}',
                        method=kwargs['method'], endpoint=endpoint, status=status_str))
        self.__status_code = response.status_code
        if not self.__is_ok:
            logging.error(_(msg="Request response: {response}", response=response.text))
        return self.__is_ok, response.text


    def submit2(self, **kwargs):
        """
        It's an improved version of 'submit()' it returns a tuple of three
            (status, content, message)
        If status is not True:
            - content returns a status code (http code)
            - message returns a response from backend
        If status is True:
            - content returns content
            - message return a message about the success
        """
        good_codes = kwargs.pop('good_http_codes')
        endpoint = kwargs.pop('endpoint')
        url = '{0}{1}'.format(self.url, endpoint)
        payload = kwargs.pop('payload') if 'payload' in kwargs.keys() else {}
        response = self.request(url=url, json=payload, **kwargs)
        self.__is_ok = response.status_code in good_codes
        status_str = ('SUCCESS({status_code})' if self.__is_ok else 'FAIL({status_code})').format(status_code=response.status_code)
        logging.debug(_(msg='Request {method} on {endpoint} completed with {status}',
                        method=kwargs['method'], endpoint=endpoint, status=status_str))
        self.__status_code = response.status_code
        message = "Request {method} completed with {status}".format(method=kwargs['method'].upper(),
                                                                    status=status_str)
        content = None
        if not self.__is_ok:
            logging.error(_(msg="Request response: {response}", response=response.text))
            content = response.status_code
        else:
            content = response.text
        return self.__is_ok, content, message



    @property
    def is_ok(self):
        return self.__is_ok


class PKIProxyClient(BackendBase):
    def __init__(self, config):
        super().__init__(url=config['endpoint']['url'], config=config)

    def pkcs12_data(self, authtoken, cert_params):
        return self.__download_file(authtoken, 'container_pkcs12', cert_params)

    def pkcs12_passcode(self, authtoken, cert_params):
        return self.__download_file(authtoken, 'container_pkcs12_passcode', cert_params)

    def __download_file(self, authtoken, file_kind, cert_params):
        filename_pattern = self.config['filename_patterns'][file_kind]
        filename = filename_pattern.format(**cert_params)
        success, result = self.send(method='GET', url=filename, payload={})
        if success:
            return success, result.content.read()
        else:
            logging.error(_(msg="File '{filename}' has not been downloaded. Error message: {error})",
                            filename=filename, error=response.text))
        return success, None, "File has not been downloaded: {}".format(response.text)


class PKIServiceClient(BackendBase):
    pass


class CertificateManager(object):

    def __init__(self, pkiproxy, pkiservice, vaultservice):
        self.pkiproxy = pkiproxy
        self.pkiservice = pkiservice
        self.vaultservice = vaultservice

    def __certsecret(self, fqdn, store_format):
        """
        it returns a tuple of three:
        (the vaultsecret object instance, namespace_params, namespace_pattern)
        """
        return VaultSecret2(http_client=self.vaultservice,
                            prefix=self.vaultservice.config['prefixes']['certificate_secrets']), \
               {'fqdn': fqdn, 'store_format': store_format}, \
               self.vaultservice.config['patterns']['certificate_namespace']

    def delete_certificate(self, authtoken, fqdn, kind):
        secret, ns_params, ns_pattern = self.__certsecret(fqdn, kind)
        return secret.delete(namespace_params=ns_params,
                             payload={},
                             namespace_pattern=ns_pattern)


    def write_certificate(self, authtoken, fqdn, kind, store_format, content, passcode):
        """
        - store_format describes in what form the content should be stored in Vault.
        Supported values: plain, base64
        - kind tells what kind of certificate it is.
        Supported values: ca-bundle, crt, key, pkcs12, jks
        - content is the certificate itself. It should be in either plain or base64 encoded
        """
        if content is None:
            msg = "Provided certificate is empty. It will not be uploaded into store"
            logging.error(_(msg=msg))
            return False, None, msg

        secret, ns_params, ns_pattern = self.__certsecret(fqdn, kind)
        payload = {'kind': kind, 'fqdn': fqdn, 'content': content,
                   'store_format': store_format}
        if passcode is not None:
            payload.update({'passcode': passcode})
        ok, content, message = secret.write(namespace_params=ns_params,
                                            payload={'data': payload},
                                            namespace_pattern=ns_pattern)
        if ok:
            message = "Certificate {fqdn}(store:{kind}) uploaded to store".format(
                    fqdn=fqdn, kind=kind)
            logging.info(_(msg=message))
        return ok, content, message



    def read_certificate(self, authtoken, fqdn, kind, format):
        """
        It read a certificate from Vault store and returns content
        - kind may take following values (might be extended later): ca, crt, key, pkcs, jks
        - soruce specifies whether the kind should be read as is or gets extracted from
          a specified container. Supported values:
          - direct - read it directly from the store as is
          - pkcs12 - extract it from pkcs12
          - jks - extract it from jks store
        The option 'source' does not work when kind is one of 'pkcs12', 'jks'.
        The function returns a tuple:
          (status(bool), content(dict), message(str)).
        If status is True:
            - content returns the certificate secret keys
            - message - information about successful read
        If status is False:
            - content returns None
            - message returns a message about the cause
        """
        supported_formats = ('native', 'base64', 'json', 'passcode')
        if format not in supported_formats:
            return False, None, "Requested unsupported format '{format}'. " \
                                "Chose from: {supported}".format(format=format,
                                                                 supported=supported_formats)
        secret, ns_params, ns_pattern = self.__certsecret(fqdn, kind)
        ok, content, message = secret.read(namespace_params=ns_params, namespace_pattern=ns_pattern)
        if not ok:
            if content in (404,):
                message = "Requested certificate '{fqdn}(store:{kind})' " \
                          "does not exist in the store".format(fqdn=fqdn, kind=kind)
                logging.error(_(msg=message))
        if ok:
            entry = json_loads(content)['data']['data']
            store_format = entry['store_format']
            data = entry['content']
            if format == 'json':
                content = entry
                message = "Requested certificate {fqdn}(store:{kind}) successfully retrieved " \
                          "from store".format(fqdn=fqdn, kind=kind)
                logging.info(_(msg=message))
            elif format == 'passcode':
                if 'passcode' not in entry.keys():
                    ok, content, message = \
                        False, None, "Requested certificate does not have a passcode with it"
                else:
                    content, message = entry['passcode'], "Passcode provided. Keep it in secret"
            elif format == 'base64':
                if store_format == 'base64':
                    content = data
                else:
                    content = b64decode(bytearray(data.encode()))
            elif format == 'native':
                if store_format == 'base64':
                    content = b64decode(bytearray(data.encode()))
                else:
                    content = data
        return ok, content, message



    def __read_file_from_pkiproxy(self, authtoken, cert_params):
        ok, pkcs12_data = self.pkiproxy.pkcs12_data(authtoken, cert_params)
        if not ok:
            return False, None, "Certificate container PKCS12 not found on PKI Proxy"
        ok, pkcs12_passcode = self.pkiproxy.pkcs12_passcode(authtoken, cert_params)
        if not ok:
            return False, None, "Passcode on container PKCS12 not found on PKI Proxy"

    def order_certificate(self, authoken, cert_params):
        pass




class VaultServiceClient(BackendBase):
    def __init__(self, config):
        super().__init__(url=config['endpoint']['url'], config=config,
                         headers={'X-Vault-Token': config['endpoint']['token']})

    def create_project(self, project_template, project_config):
        return self.__action_on_project('create', project_template, project_config)

    def delete_project(self, project_template, project_config):
        return self.__action_on_project('delete', project_template, project_config)

    def __action_on_project(self, action, project_template, project_config):
        secret = VaultSecret(http_client=self,
                             prefix=self.config['prefixes']['project_secrets'])
        handler = {'create': secret.write, 'delete': secret.delete}[action]

        project_template = load_config(yaml_file=project_template)

        for item in project_template.get('secrets', []):
            rendered_item = render_config(json_config=item, params=project_config)
            success, message = handler(namespace_pattern=self.config['patterns']['custom_namespace'],
                                       namespace_params={'namespace': rendered_item['namespace']},
                                       payload={'data': item['data']})
            if not success:
                return success, message
        return True, "Request {action} for the project has been completed with SUCCESS".format(action=action.upper())

    def write_projectsecret(self, authtoken, project, secret, data):
        return self.__action_on_project_secret('create', authtoken, project, secret, data)

    def read_projectsecret(self, authtoken, project, secret, data):
        return self.__action_on_project_secret('read', authtoken, project, secret, data)

    def delete_projectsecret(self, authtoken, project, secret, data):
        return self.__action_on_project_secret('delete', authtoken, project, secret, data)


    def __action_on_project_secret(self, action, authtoken, project, secret, data):
        """
        'project' and 'secret' are dictionaries with keys specified in the backend config.
        Namespace project_namespace requires following keys:
        - project, environ, component, version - are coming from 'project'
        - category, alias - are coming from 'secret'
        """
        st = VaultSecret(http_client=self,
                             prefix=self.config['prefixes']['project_secrets'])
        handler = {'create': st.write, 'delete': st.delete, 'read': st.read}[action]
        ns_params = project
        ns_params.update(secret)
        success, message = handler(namespace_pattern=self.config['patterns']['project_namespace'],
                                   namespace_params=ns_params,
                                   payload={'data': data},
                                   headers={'X-Vault-Token': authtoken})
        if not success:
                if handler == st.read and self.last_status_code in (404,):
                    message = "Requested project secret not found"
                return success, message, {}
        try:
            message = json_loads(message) if handler != st.read else json_loads(message)['data']['data']
        except:
            pass
        return True, "Request {action} on the project secret has been completed with SUCCESS".format(action=action.upper()), message



class ServiceProcessor(object):
    def __init__(self, backend_configs, project_template):
        backend_configs = load_config(json_file=backend_configs)
        self.project_template_file = project_template
        #self.__backend_configs = backend_configs
        #self.__project_template = project_template
        self.vaultservice = VaultServiceClient(config=backend_configs['vaultservice'])

    def create_project(self, project_config):
        return self.vaultservice.create_project(project_config=project_config,
                                               project_template=self.project_template_file)

    def delete_project(self, project_config):
        return self.vaultservice.delete_project(project_config=project_config,
                                               project_template=self.project_template_file)

    def write_projectsecret(self, authtoken, project, secret, data):
        return self.vaultservice.write_projectsecret(project=project,
                                                    secret=secret, data=data,
                                                    authtoken=authtoken)

    def delete_projectsecret(self, authtoken, project, secret, data):
        return self.vaultservice.delete_projectsecret(project=project,
                                                     secret=secret, data=data,
                                                     authtoken=authtoken)

    def read_projectsecret(self, authtoken, project, secret, data):
        return self.vaultservice.read_projectsecret(project=project,
                                                   secret=secret, data=data,
                                                   authtoken=authtoken)

    def order_certificate(self):
        """
        Suported options:
        - overwrite(bool) - if set True, it orders a new certificate otherwise
            if a certificate already exists, it just returns success
        """
        pass


    def read_certificate(self, authtoken, fqdn, kind, format):
        """
        Supported options: order(bool),wait(bool)
        - order(bool) - enforces to order a certificate
        - wait(bool) - (only if order=True), waits when a certificate appears in the store
          and download it
        - timeout(int) - how many seconds to wait to download a certificate
        """
        #cm = CertificateManager(self.pkiproxy, self,pkiservice, self.vaultservice)
        cm = CertificateManager(None, None, self.vaultservice)
        return cm.read_certificate(authtoken, fqdn, kind, format)

    def write_certificate(self, authtoken, fqdn, kind, content, passcode):
        """
        The content should be binary data
        """
        cm = CertificateManager(None, None, self.vaultservice)
        if not isinstance(content, bytes):
            return False, None, "Certificate content must be provided as binary data"
        text_content, store_format = None, ''
        try:
            text_content = content.decode('utf-8')
            store_format = 'plain'
            logging.debug(_(msg="Certificate '{fqdn}({kind})' has been provided in the text form",
                            fqdn=fqdn, kind=kind))
        except:
            text_content = encodebytes_2base64(content).decode()
            store_format = 'base64'
            logging.debug(_(msg="Certificate '{fqdn}({kind})' was converted to 'base64'",
                            fqdn=fqdn, kind=kind))
        return cm.write_certificate(authtoken, fqdn, kind, store_format, text_content, passcode)


    def delete_certificate(self, authtoken, fqdn, kind):
        cm = CertificateManager(None, None, self.vaultservice)
        return cm.delete_certificate(authtoken, fqdn, kind)


def main_cli():

    backend_configs = os.environ.get('BACKEND_CONFIGS', 'backend-configs.json')
    project_template = os.environ.get('PROJECT_TEMPLATE', 'project-template.yaml')

    actions = ('create-project', 'delete-project',
               'read-cert', 'order-cert', 'delete-cert', 'upload-cert',
               'write-project-secret', 'read-project-secret', 'delete-project-secret')
    parser = argparse.ArgumentParser(description='Project data provisioner')
    parser.add_argument('-a', '--action', choices=actions, help='Chose an action to perform')

    cfg_opts = parser.add_argument_group('Common options')
    cfg_opts.add_argument('--backend-configs', default=backend_configs,
                          help='JSON file with backend configurations. Default: {}'.format(backend_configs))
    cfg_opts.add_argument('--project-template', default=project_template,
                          help='JSON file with project template. Default: {}'.format(project_template))

    project_opts = parser.add_argument_group('Project options')
    project_opts.add_argument('--project', help='Project name')
    project_opts.add_argument('--environ', help='Environment name')
    project_opts.add_argument('--component', help='Component name')
    project_opts.add_argument('--version', help='Component version')
    project_opts.add_argument('--project-config', help='Project config from a JSON file (replaces --project, --environ, --component, and --version')
    project_opts.add_argument('--secret-category', help='Project secret category')
    project_opts.add_argument('--secret-alias', help='Project secret alias')
    project_opts.add_argument('--secret-data', default='{}', help='Data for secret')


    cert_opts = parser.add_argument_group('Certificate options')
    cert_opts.add_argument('--cert-fqdn', help="Certificate fqdn")
    cert_opts.add_argument('--cert-fqdn-aliases', help="FQDN aliases for ordering a certificatefqdn")
    cert_opts.add_argument('--cert-kind', help="Certificate type (crt, key, pkcs12, jks, ca-bundle")
    cert_opts.add_argument('--cert-file', help="Path to a file to upload it for upload-cert action")
    cert_opts.add_argument('--cert-passcode', help="Provide passcode to upload it by upload-cert action")

    args = parser.parse_args()


    service = ServiceProcessor(backend_configs=args.backend_configs,
                               project_template=args.project_template)
    if args.action in ('create-project', 'delete-project'):
        action_handler = {
            'create-project': service.create_project,
            'delete-project': service.delete_project
        }.get(args.action)
        action_handler(load_config(yaml_file=args.project_config))
    elif args.action in ('read-cert', 'order-cert', 'delete-cert', 'upload-cert'):
        req_args = [args.cert_fqdn,]
        if args.action in ('read-cert', 'delete-cert'):
            req_args.append(args.cert_store_format)
        elif args.action in ('upload-cert',):
            req_args.append(args.cert_file)

        if not all(req_args):
            parser.error("Not provided all required arguments")

        if args.action == 'read-cert':
            service.read_certificate('Token', args.cert_fqdn, args.cert_kind)
        elif args.action == 'upload-cert':
            if not os.path.exists(args.cert_file):
                parser.error("Certificate file {} must exist".format(args.cert_file))
            with open(args.cert_file, 'rb') as fd:
                service.write_certificate('Token', args.cert_fqdn, args.cert_kind,
                                           fd.read(), args.cert_passcode)


    elif args.action in ('write-project-secret', 'read-project-secret', 'delete-project-secret'):
        action_handler = {
            'write-project-secret': service.write_projectsecret,
            'read-project-secret': service.read_projectsecret,
            'delete-project-secret': service.delete_projectsecret
        }.get(args.action)
        json_data = json_loads(args.secret_data)
        result = action_handler('', load_config(yaml_file=args.project_config),
                                secret={'category': args.secret_category, 'alias': args.secret_alias},
                                data=json_data)
        success, message, data = result
        if success and action_handler == service.read_projectsecret:
            print(data)


if __name__ == "__main__":
    main_cli()
    pass

