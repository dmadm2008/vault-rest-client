#!/usr/bin/env python

import os
import json
import logging
import requests
import argparse


def _(**kwargs):
    print(kwargs['msg'].format(**kwargs))

def validate_params(**kwargs):
    pass



class HTTPClient(object):
    def __init__(self, **kwargs):
        validate_params(optional=('url', 'method', 'headers', 'payload', 'good_http_codes', 'username', 'password'), params=kwargs)
        self.__session = requests.Session()
        self.__params = kwargs
        self.__ok = None
        if self.__params.get('method') is None:
            self.__params['method'] = 'GET'
        self.__session.headers.update(kwargs.get('headers', {}))
        self.__session.auth = (kwargs.get('username'), kwargs.get('password'))

    def request(self, **kwargs):
        validate_params(optional=('url', 'method', 'headers', 'payload', 'good_http_codes', 'username', 'password'), params=kwargs)
        __old_headers = self.__session.headers
        self.__session.headers.update(kwargs.get('headers', {}))

        url = kwargs.get('url', self.__params.get('url'))
        assert(url is not None), "URL cannot be NoneType"
        method=kwargs.get('method', self.__params['method'])

        auth=(kwargs.get('username'), kwargs.get('password'))
        __old_auth = self.__session.auth

        if all(auth):
            self.__session.auth = auth

        __attempts = 3
        while __attempts > 0:
            try:
                self.__response = self.__session.request(url=url, method=method,
                    json=kwargs.get('payload', self.__params.get('payload', {})))

                break
            except Exception as exc:
                logging.debug(_(msg="Request {method} on {endpoint} failed with: {error}",
                                method=method.upper(), endpoint=url, error=exc))
                __attempts -= 1

        logging.info(_(msg="Request {method} on {endpoint} completed with code {status_code}",
                       endpoint=url, method=method.upper(), status_code=self.__response.status_code))
        self.__ok = (self.__response.status_code in kwargs.get('good_http_codes', (200,)))

        if kwargs.get('headers') is not None:
            self.__session.headers = __old_headers
        if __old_auth:
            self.__session.auth = __old_auth
        return self.__ok

    def response(self):
        return self.__response

    def json(self):
        return self.__response.json()

    def status_code(self):
        return self.__response.status_code

    def is_ok(self):
        return self.__ok


class PKIServiceClient(object):
    def __init__(self, **kwargs):
        validate_params(required=('endpoint', 'auth_token', 'payload'), params=kwargs)
        self.__params = kwargs
        self.__httpclient = HTTPClient(endpoint=kwargs['payload'], method='POST',
            headers={'OAuth-Token': kwargs['payload'], 'Content-Type': 'application/json'})

    def request_certificates(self):
        return self.__httpclient.request(good_http_codes=(200,), payload=self.__params['payload'])


class VaultClient(object):
    __api_endpoints = {
        'username_login': (
            ('login', 'POST', '/auth/{username_engine}/login/{username}'),
            ('logout', 'POST', '/auth/{username_engine}/logout/{username}'),
        ),
        'approle': (
            ('create', 'PUT', '/auth/{approle_engine}/{approle}'),
            ('delete', 'DELETE', '/auth/{approle_engine}/{approle}'),
            ('role_id', 'POST', '/auth/{approle_engine}/role/{approle}/role-id'),
            ('secret_id', 'PUT', '/auth/{approle_engine}/role/{approle}/secret-id'),
        ),
        'secret': (

        ),
        'policy': (

        )
    }
    def __init__(self, **kwargs):
        validate_params(required=('endpoint',),
                        optional=('token', 'username', 'password'), params=kwargs)
        self.__httpclient = HTTPClient(endpoint=kwargs['endpoint'])
        self.__params = kwargs
        self.__auth_token = None

    def login(self):
        if self.__params.get('token') is not None:
            self.__auth_token = self.__params['token']
            return True
        elif all([self.__params.get('username'), self.__params.get('password')]):
            # TODO: complete the login procedure
            self.__httpclient = None
        else:
            logging.error(_(msg="Either 'token' or 'username' and 'password' should be provided"))
    pass


class PKIProxyClient(object):
    """
    Client to the PKI proxy
    """
    __source_patterns = (
        ('pkcs12',              '{server}_{fqdn}_certificate.p12'),
        ('jks',                 '{server}_{fqdn}_certificate.jks'),
        ('pkcs12_properties',   '{server}_{fqdn}_certificate.p12-properties'),
        ('password',            '{server}_{fqdn}.password'),
    )
    def __init__(self, **kwargs):
        """
        project_attrs should contain the variables defined in the __source_patterns such as
        {
            'server': 'target server',
            'fqdn': 'fqdn of the requested certificates'
        }
        """
        validate_params(required=('endpoint', 'username', 'password', 'project_attrs'), params=kwargs)
        validate_params(required=('server', 'fqdn'), params=kwargs['project_attrs'])
        self.__params = kwargs
        self.__httpclient = HTTPClient(headers={'Content-Type': 'application/type'},
                                       method='GET', username=kwargs['username'],
                                       password=kwargs['password'])

    def download_all(self):
        return ((kind, self.download(kind)) for kind, pattern in self.__source_patterns)

    def download(self, kind):
        pattern = list(filter(lambda entry: entry[0] == kind, self.__source_patterns))[0]
        params = {
            'endpoint': self.__params['endpoint'],
            'source': pattern[1].format(**self.__params['project_attrs'])
        }
        url = "{endpoint}/{source}".format(**params)
        if self.__httpclient.request(url=url):
            return self.__httpclient.json()
    pass


class ProjectProvisioner(object):
    def __init__(self, **kwargs):
        pass
    pass


def test_http_client(**kwargs):
    params = {
        'url': 'http://worldtimeapi.org/api/ip',
    }
    proxy_config = {
        'endpoint': 'https://httpbin.org/headers',
        'username': 'myuser',
        'password': 'mypass',
        'project_attrs': { 'fqdn': 'mywebsite.com', 'server': 'usdl90007' }
    }
    proxy = PKIProxyClient(**proxy_config)
    for f in proxy.download_all():
        print(f)


def main_cli():
    parser = argparse.ArgumentParser()
    actions = ('testhttp',)
    parser.add_argument('-a', choices=actions, help='Select an action to perform')
    args = parser.parse_args()

    if args.a == 'testhttp':
        test_http_client()
    pass

if __name__ == "__main__":
    main_cli()

