#!/usr/bin/env python

import os
import argparse
import logging

from OpenSSL import crypto as ssl_crypto
from OpenSSL.crypto import FILETYPE_PEM, dump_privatekey, dump_certificate


openssl_exec = "/usr/bin/openssl"
passcode = 'test'

class OpenSSLClient(object):
    def __init__(self, pkcs12_data, jks_data, passcode):
        self.__passcode = passcode
        self.__pkcs12_data = pkcs12_data
        self.__jks_data = jks_data
        self.__certs = {}
        try:
            self.__pkcs12 = ssl_crypto.load_pkcs12(self.__pkcs12_data, passcode)
        except Exception as exc:
            logging.error("Cannot load the given PKCS12 container: '{0}'".format(exc))

    def __wrapper(self, msg, func, *args):
        try:
            return func(*args)
        except Exception as exc:
            logging.error("Cannot process '{0}': {1}".format(msg, exc))
        return b''

    def extract(self):
        if not self.__pkcs12_data:
            return
        dump_key = FILETYPE_PEM
        subject = self.__pkcs12.get_ca_certificates()[0]
        print(subject.get_subject())
        self.__certs = {
            'cacert': {
                'kind': 'ca_certificate',
                'content': self.__wrapper('ca_certificate',
                            dump_certificate, dump_key, self.__pkcs12.get_ca_certificates()[0]).decode(),
                'content_encoded': 'plain'
            },
            'cert': {
                'kind': 'certificate',
                'content': self.__wrapper('certificate',
                            dump_certificate, dump_key, self.__pkcs12.get_certificate()).decode(),
                'content_encoded': 'plain'
            },
            'rsa': {
                'kind': 'private_key',
                'content': self.__wrapper('certificate',
                            dump_privatekey, dump_key, self.__pkcs12.get_privatekey()).decode(),
                'content_encoded': 'plain'
            },
            'pkcs12': {
                'kind': 'pkcs12',
                'content': encodebytes(self.__pkcs12_data).decode(),
                'content_encoded': 'base64',
                'passcode': self.__passcode
            },
            'jks': {
                'kind': 'jks',
                'content': encodebytes(self.__jks_data).decode(),
                'content_encoded': 'base64',
                'passcode': self.__passcode
            }
        }
        return self.__certs



def main_cli():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--pkcs12', default='/tmp/certs/cert.p12', help='Source container in PKCS12 format')
    parser.add_argument('-j', '--jks', default='/tmp/certs/cert.jks', help='Source container in JKS format')
    parser.add_argument('-c', '--passcode', default='test', help='Passcode to the PKCS12')
    parser.add_argument('-o', '--output', help='Formats to convert to separated by a comma')

    args = parser.parse_args()

    if os.path.exists(args.pkcs12):
        pkcs12_data = open(args.pkcs12, 'rb').read()
        ssl = OpenSSLClient(pkcs12_data, b'', args.passcode)
        print(ssl.extract())


if __name__ == "__main__":
    main_cli()
    pass

