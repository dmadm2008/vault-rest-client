#!/usr/bin/env python

#
# -*- coding: utf-8
#

import logging
import argparse
from services.vault.core import VaultService, VaultProjectSecret
from services.vault.models import (
    PolicyModel,
    ProjectSecretModel,
)


vs_config = dict(
    endpoint = dict(
        url = 'http://127.0.0.1:8200',
        token = 's.OGnIJi5U4rNGuwev0XAPeGim',
        unseal_keys = [
            "CGaz0tHbr9udyGR0RW2ZepzLIG5KvFqfNARXcsK26rE3",
            "mXX71o38eytL3gPgoNR0sDqF4e4Yz3ub6ml/tz3hFQwa",
            "VTSx7aj2EuK1J52YEEut/rwiH3oZsoO1+IPIKimG/zK9",
            "nnmz3aoDrvvMcjnRRshpRAdks9oSzsCgt+UIuLmU89dW",
            "3iHXdtsD7BY7F9DnUU3I7iRm3swBGF0Ta437VclYsp9Y"
        ]
    ),
    api_endpoints = dict(
        ping = '/ping',
        install = '/installCert',
        detail = '/getDetails'
    ),
    namespaces = dict(
        project_approles_prefix = '/v1/auth/project-approles',
        project_approles_pattern = '/role/{role_name}',
        project_secrets_prefix = '/v1/projects',
        project_secrets_pattern = '/{project}/{environ}/{component}/{version}/{category}/{alias}',
        policy_prefix = '/v1/sys/policy',
        policy_pattern = '/{name}'
    ),
    project = dict(categories = ["accounts", "envvars", "settings"])
)

pkiproxy_config = dict(
    endpoint = dict(
        url = 'http://127.0.0.1:8080',
        token = 'sdfadsfsd'
    ),
    filename_patterns = dict(
        container_pkcs12: 'WEBSERVER_{fqdn}_T.pkcs12',
        container_pkcs12_passcode: 'WEBSERVER_{fqdn}_T.pkcs12.passcode',
    )
)


def test_vaultapprole():
    approle_ex = dict(role_name='myproject-dev-web', policies=['default','myproject-dev-web-reader'])
    vs = VaultService(vs_config)
    vs.project_approle.create(**approle_ex)
    req_roleid = vs.project_approle.role_id(approle_ex['role_name'])
    req_secretid = vs.project_approle.secret_id(approle_ex['role_name'])

    if req_roleid.success and req_secretid.success:
        token = vs.project_approle.login(req_roleid.content, req_secretid.content)
        # print("TOKEN", token)
    vs.project_approle.delete(approle_ex['role_name'])


def test_vaultpolicy():
    vs = VaultService(vs_config)
    policy = dict(name = "autocreatedpolicy", policy= """
        path "/certificates/*" {
            capabilities = [ "read", "list", "create", "delete" ]
        }
    """)
    vs.policy.create(**policy)
    vs.policy.delete(policy['name'])
    pass


def test_vaultsecret():
    secret = dict(
        project = "myproject",
        environ = "dev",
        component = "apicore",
        version = "beta1",
        category = "envvars",
        alias = "default",
        data = dict(
            MONGODB_CONN = "mongodb://127.0.0.1:27017",
            MONGODB_USER = "mongouser",
            MONGODB_PASS = "mongopass"
        ),
        metadata = dict()
    )
    vs = VaultService(vs_config)
    vs.project_secret.create(**secret)
    vs.project_secret.delete(**secret)


def test_projectcreate():
    vs = VaultService(vs_config)
    project = dict(
        project="myproject",
        environ="dev",
        component="scheduler",
        version="beta1"
    )
    vs.create_project_namespace("project-template.yaml", project)
    vs.delete_project_namespace("project-template.yaml", project)


def main_cli():
    logger = logging.getLogger(__name__)
    logging.basicConfig(level=logging.INFO, format="%(levelname)s %(message)s")
    parser = argparse.ArgumentParser()
    log_levels = dict(info=logging.INFO, debug=logging.DEBUG, warn=logging.WARNING,
                      error=logging.ERROR, crit=logging.CRITICAL)
    parser.add_argument('-l', '--log-level', default='info',
                        choices=log_levels.keys(),
                        help=f"Logging level (default: info)")
    args = parser.parse_args()
    logger.setLevel(log_levels[args.log_level])


if __name__ == "__main__":
    # test_vaultapprole()
    # test_vaultpolicy()
    # test_vaultsecret()
    main_cli()
    test_projectcreate()

