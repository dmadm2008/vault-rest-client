#!/usr/bin/env python
#
# -*- coding: utf-8
#


from json import load as json_load
from fastapi import Depends, FastAPI, APIRouter, Header, HTTPException
from services.vault.core import VaultService
from services.vault.models import ProjectSecretModel, ReadSecretModel


app = FastAPI()
project_router = APIRouter()
secretproject_router = APIRouter()
certs_router = APIRouter()

squirrel_config = None
with open("squirrel-config.json", "rb") as fd:
    squirrel_config = json_load(fd)
project_template_file = "project-template.yaml"



async def get_token_header(x_auth_token: str = Header(...)):
    if x_auth_token is None or x_auth_token == "":
        raise HTTPException(status_code=400, detail="X-Auth-Token header invalid")



#
# =============================================================================
# Create/Delete project (begin)
# =============================================================================
#
@project_router.post("/{project}/{environ}/{component}/{version}")
async def create_project(project, environ, component, version,
                         x_auth_token: str = Header(...)):
    vault = VaultService(squirrel_config['vaultservice'], x_auth_token)
    project = dict(project=project, environ=environ,
                   component=component, version=version)
    return vault.create_project_namespace(project_template_file, project)



@project_router.delete("/{project}/{environ}/{component}/{version}")
async def delete_project(project, environ, component, version,
                         x_auth_token: str = Header(...)):
    vault = VaultService(squirrel_config['vaultservice'], x_auth_token)
    project = dict(project=project, environ=environ,
                   component=component, version=version)
    return vault.delete_project_namespace(project_template_file, project)
#
# Create/Delete project (end)
# =============================================================================
#



#
# =============================================================================
# Create/Delete project secret (begin)
# =============================================================================
#
@secretproject_router.post("/{project}/{environ}/{component}/{version}/{category}/{alias:path}")
async def create_project_secret(project, environ, component, version, category, alias,
                               data: ProjectSecretModel,
                               x_auth_token: str = Header(...)):
    vault = VaultService(squirrel_config['vaultservice'], x_auth_token)
    secret = dict(project=project, environ=environ, category=category,
                  component=component, version=version, alias=alias, data=data)
    return vault.project_secret.create(secret)



@secretproject_router.get("/{project}/{environ}/{component}/{version}/{category}/{alias:path}")
async def read_project_secret(project, environ, component, version, category, alias,
                                data: ReadSecretModel,
                                x_auth_token: str = Header(...)):
    vault = VaultService(squirrel_config['vaultservice'], x_auth_token)
    secret = dict(project=project, environ=environ, category=category,
                  component=component, version=version, alias=alias)
    return vault.project_secret.delete(secret)



@secretproject_router.delete("/{project}/{environ}/{component}/{version}/{category}/{alias:path}")
async def delete_project_secret(project, environ, component, version, category, alias,
                               x_auth_token: str = Header(...)):
    vault = VaultService(squirrel_config['vaultservice'], x_auth_token)
    secret = dict(project=project, environ=environ, category=category,
                  component=component, version=version, alias=alias)
    return vault.project_secret.delete(secret)
#
# Create/Delete project secret (end)
# =============================================================================
#



#
# =============================================================================
# Create/Delete/Order certificates (begin)
# =============================================================================
#
@certs_router.post("/certificate/by-fqdn/{fqdn}/{kind}")
async def write_certificate_by_fqdn(fqdn: str, kind: str,
                                    data: ProjectSecretModel,
                                    x_auth_token: str = Header(...)):
    #vault = VaultService(squirrel_config['vaultservice'], x_auth_token)
    #secret = dict(project=project, environ=environ, category=category,
    #              component=component, version=version, alias=alias, data=data)
    #return vault.project_secret.create(secret)
    return



@certs_router.get("/by-fqdn/{fqdn}/{kind}")
async def read_certificate_by_fqdn(fqdn: str, kind, str,
                                   data: ReadSecretModel,
                                   x_auth_token: str = Header(...)):
    vault = VaultService(squirrel_config['vaultservice'], x_auth_token)
    secret = dict(project=project, environ=environ, category=category,
                  component=component, version=version, alias=alias)
    return vault.project_secret.delete(secret)



@certs_router.delete("/by-fqdn/{fqdn}/{kind}")
async def delete_certificate_by_fqdn(fqdn: str, kind: str,
                                     x_auth_token: str = Header(...)):
    vault = VaultService(squirrel_config['vaultservice'], x_auth_token)
    secret = dict(project=project, environ=environ, category=category,
                  component=component, version=version, alias=alias)
    return vault.project_secret.delete(secret)



@certs_router.post("/order-by-fqdn/{fqdn}")
async def order_certificate_by_fqdn(fqdn: str,
                                     x_auth_token: str = Header(...)):
    vault = VaultService(squirrel_config['vaultservice'], x_auth_token)
    secret = dict(project=project, environ=environ, category=category,
                  component=component, version=version, alias=alias)
    return vault.project_secret.delete(secret)
#
# Create/Delete/Order certificates (end)
# =============================================================================
#





app.include_router(project_router,
                   prefix="/v1/projects",
                   tags=["Projects",],
                   dependencies=[Depends(get_token_header)],
                   responses={404: {'description': 'Not found'}})

app.include_router(secretproject_router,
                   prefix="/v1/projects",
                   tags=["Project data",],
                   dependencies=[Depends(get_token_header)],
                   responses={404: {'description': 'Not found'}})

app.include_router(certs_router,
                   prefix="/v1/certificate",
                   tags=["Certificates",],
                   dependencies=[Depends(get_token_header)],
                   responses={404: {'description': 'Not found'}})


