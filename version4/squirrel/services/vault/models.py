#
# -*- coding: utf-8
#

from pydantic import BaseModel, HttpUrl
from typing import List, Any, Dict


#
# VaultService models
#


class ConfigEndpointModel(BaseModel):
    url: HttpUrl
    token: str = ""
    unseal_keys: List[str] = []



class ConfigApiEndpointsModel(BaseModel):
    ping: str
    install: str
    detail: str



class ConfigNamespacesModel(BaseModel):
    project_approles_prefix: str
    project_approles_pattern: str
    project_secrets_prefix: str
    project_secrets_pattern: str
    policy_prefix: str
    policy_pattern: str


class ServiceConfigModel(BaseModel):
    """
    The model describes the config that should be provided
    """
    endpoint: ConfigEndpointModel
    api_endpoints: ConfigApiEndpointsModel
    namespaces: ConfigNamespacesModel


#
# AppRole models
#


class AppRoleModel(BaseModel):
    role_name: str



class CreationAppRoleModel(AppRoleModel):
    policies: List[str] = []



class ApiFuncModel(BaseModel):
    method: str
    endpoint: str



#
# Secret model
#

class ReadSecretModel(BaseModel):
    project: str
    environ: str
    component: str
    version: str
    category: str = None
    alias: str = None



class SecretModel(BaseModel):
    data: Dict = None
    metadata: Dict = None



class ProjectSecretModel(SecretModel, ReadSecretModel):
    pass


class CustomSecretModel(SecretModel):
    namespace: str



#
# Policy models
#


class PolicyModel(BaseModel):
    name: str
    policy: str

