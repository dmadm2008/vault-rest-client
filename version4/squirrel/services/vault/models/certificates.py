#
# -*- coding: utf-8
#

import abc
from pydantic import (
    BaseModel,
    SecretStr,
    validator
)
import base64
from enum import Enum, IntEnum, unique
from typing import List, Union



def bytes2base64(data):
    assert(isinstance(data, bytes)), "Data for encoding to base64 must be binary data"
    return base64.b64encode(data).decode('utf-8')

@unique
class Kind(str, Enum):
    CERT = 'cert'
    CACERT = 'cacert'
    RSAKEY = 'key'
    PKCS12 = 'pkcs12'
    JKS = 'jks'

@unique
class StoreFormat(str, Enum):
    base64 = 'base64'
    text = 'plain'

@unique
class DataFormat(str, Enum):
    native = 'native'
    text = 'plain'

class ContentFormat(str, Enum):
    binary = 'binary'
    text = 'plain'

#
# Abstract classes
#
class CertificateBase(BaseModel, abc.ABC):
    kind: Kind
    content: bytes                  # actual content is always bytes
    content_format: ContentFormat   # format in what the content is represented: binary or text
    store_format: StoreFormat       # format in what the content is being stored in the store: plain or base64
    fqdn: str

    @abc.abstractmethod
    def data(cls, format: DataFormat): pass

    @property
    def native_content(self):
        return self.data(DataFormat.native)

    def check_content_type(cls, v):
        assert(isinstance(v, bytes)), 'Content type must be a str but given ' + type(v)
        return v

    def serialize(self):
        return self.json(exclude={'content_format'})



class ContainerCertificateBase(CertificateBase):
    def __init__(self, **kwargs):
        super().__init__(content_format=ContentFormat.binary, store_format=StoreFormat.base64, **kwargs)
    passcode: SecretStr

    def data(self, format: DataFormat):
        if format == DataFormat.native:
            return self.content
        elif format == DataFormat.text:
            return bytes2base64(self.content)
        else:
            raise Exception("Requested unexpected format")

    @validator('passcode')
    def passcode_must_notbe_empty(cls, v):
        assert(v not in (None, '')), "Passcode cannot be empty"
        return v


    class Config:
        json_encoders = {
            bytes: bytes2base64
        }


class X509CertificateBase(CertificateBase):
    def __init__(self, **kwargs):
        super().__init__(content_format=ContentFormat.text, store_format=StoreFormat.text, **kwargs)

    def data(self, format: DataFormat):
        return self.content.decode('utf-8')



#
#
# Specific certificate implementations
#
# Examples of creating instances of the models:
#   rsa = RSAKeyCertificate(fqdn='example.com', content='BEGIN RSA PRIVATE KEY --- ...')
#   cacert = CACertCertificate(fqdn='example.com', content='BEGIN CERTIFICATE --- ...')
#   pkcs12 = PKCS12Certificate(fqdn='example.com', content=b'binarycontent', passcode='certpass')
#
#
class PKCS12Certificate(ContainerCertificateBase):
    def __init__(self, **kwargs):
        super().__init__(kind=Kind.PKCS12, **kwargs)


class JKSCertificate(ContainerCertificateBase):
    def __init__(self, **kwargs):
        super().__init__(kind=Kind.JKS, **kwargs)


class CACertCertificate(X509CertificateBase):
    def __init__(self, **kwargs):
        super().__init__(kind=Kind.CACERT, **kwargs)


class CertCertificate(X509CertificateBase):
    def __init__(self, **kwargs):
        super().__init__(kind=Kind.CERT, **kwargs)


class RSAKeyCertificate(X509CertificateBase):
    def __init__(self, **kwargs):
        super().__init__(kind=Kind.RSAKEY, **kwargs)



if __name__ == '__main__':
    pkcs12 = PKCS12Certificate(content=b'bindatyrdata', passcode='123', fqdn='example.com')
    jks = JKSCertificate(content=b'binarydata3', passcode='123', fqdn='example.com')
#    key = RSAKeyCertificate(content='23123')
    cacert = CACertCertificate(content='thisshouldbeastring', fqdn='example.com')
    key = RSAKeyCertificate(content='BEGIN PRIVATE KEY ---\nasfsdfasdfdsfds', fqdn='example.com')
    print(pkcs12.dict())
    print(jks.dict())

#    print(jks.data(DataFormat.text))
    print(jks.json())
    print(jks.serialize())
    print(jks.native_content, jks.passcode)
    print(cacert.native_content)
    print(cacert.serialize())
    print(key.json())
    print(key.dict())
    print(key.serialize())
#    print(key.dict())
 #   print(cacert.dict())
    # print(jks.json())
    pass

