#
# -*- coding: utf-8
#

import os
import yaml
import logging
from typing import List
from services.generic.core import ResponseContentFormat
from services.generic.models import HttpClientResponseModel

from .models import (
    CreationAppRoleModel,
    ProjectSecretModel,
    PolicyModel
)



def get_project_template(template_filepath, params):
    """
    Read the template from a file, applies to it provided
    parameters (values) and returns a tuple
        (success, value).
    If success is true, value is the dictionary with rendered template.
    Otherwise the value contains the message with the error.
    """
    yaml_template = None
    try:
        t_fd = open(template_filepath, 'r')
        yaml_template = yaml.load(t_fd, Loader=yaml.SafeLoader)
        t_fd.close()
    except Exception as exc:
        msg = f"Could not load the YAML template: {exc}"
        logging.error(msg)
        return False, msg

    def render(config, params):
        if isinstance(config, str):
            return config.format(**params)
        elif isinstance(config, (bool, int)):
            return config
        elif isinstance(config, (list, tuple)):
            return [render(v, params) for v in config]
        elif isinstance(config, dict):
            return {k: render(v, params) for k, v in config.items()}
        else:
            raise Exception("You should have never seen that message. Contact to support")
    return True, render(yaml_template, params)



def create_project_namespace(template_filepath, params, controllers):
    return action_on_project_namespace('create', template_filepath, params, controllers)



def delete_project_namespace(template_filepath, params, controllers):
    return action_on_project_namespace('delete', template_filepath, params, controllers)



def action_on_project_namespace(operation, template_filepath, params, controllers):
    """
    It provisions the resources into vault store from specified in the template
    """
    is_ready, data = get_project_template(template_filepath, params)
    if not is_ready:
        return HttpClientResponseModel(success=is_ready, http_code=0,
                                       content=None, message=data)

    response = None
    for controller_name, items in data.items():
        controller = controllers.get(controller_name)
        if not controller:
            raise NotImplementedError(f"The required controller {controller_name} not found")
        for item in items:
            controller_params = None
            if isinstance(controller, VaultProjectSecret):
                controller_model = ProjectSecretModel(
                                        data=item['data'],
                                        category=item['category'],
                                        alias=item['alias'], **params)
                if operation == 'create':
                    response = controller.create(controller_model)
                else:
                    response = controller.delete(controller_model)
            elif isinstance(controller, VaultPolicy):
                controller_model = PolicyModel(**item)
                if operation == 'create':
                    response = controller.create(controller_model)
                else:
                    response = controller.delete(controller_model)
            elif isinstance(controller, VaultAppRole):
                controller_model = CreationAppRoleModel(**item)
                if operation == 'create':
                    response = controller.create(controller_model)
                else:
                    response = controller.delete(controller_model)
            else:
                raise NotImplementedError("Ooops... something has to be implemented")
            if not response.success:
                logging.error(f"Breaking the {operation} process due to previous error")
                response.message = f"Request {operation} on the project has been stopped " \
                                    "due to the error: " + response.message
                return response
    return response



class VaultKindBase(object):
    """
    Base class to create object by engine, role, etc
    """
    def __init__(self, httpclient, engine_prefix):
        self.httpclient = httpclient
        self.engine_prefix = engine_prefix



class VaultInit(VaultKindBase):
    """
    Implements the function to unseal vault
    """
    def __init__(self, httpclient):
        super().__init__(httpclient, "/v1/sys")


    def is_sealed(self):
        endpoint = f"{self.engine_prefix}/health"
        response = self.httpclient.call(
            http_good_codes=(200, 429, 472, 473, 501, 503),
            return_as=ResponseContentFormat.json,
            method='GET', endpoint=endpoint)

        return response.success and \
               (not response.content['sealed']) and \
               response.content['initialized']


    def unseal(self, keys: List[str]):
        """
        Performs the unsealing. It takes the list of keys to unseal
        """
        if len(keys) < 1:
            return HttpClientResponseModel(
                success=False, http_code=0, content=None,
                message="Keys to unseal the Vault store are not provided"
            )

        def unseal(key):
            payload = dict(key=key)
            endpoint = f"{self.engine_prefix}/unseal"
            return self.httpclient.call(
                http_good_codes=(200,), return_as=ResponseContentFormat.json,
                method='PUT', endpoint=endpoint, json=payload)
        last_unseal = None
        for key in keys:
            last_unseal = unseal(key)
            if last_unseal.success:
                if last_unseal.content['sealed'] == False:
                    last_unseal.message = f"The Vault store has successfully been unsealed.{last_unseal.message}"
                    break
        else:
            last_unseal.message = f"The Vault wasn't unseal. Details: {last_unseal.message}"
        return last_unseal



class VaultPolicy(VaultKindBase):
    """
    Implements functions to work with policies
    """
    def __init__(self, httpclient, engine_prefix, endpoint_pattern):
        super().__init__(httpclient, engine_prefix)
        self.endpoint_pattern = endpoint_pattern


    def create(self, instance: PolicyModel=None, **kwargs) -> ResponseContentFormat:
        """
        Creates a policy. Pass an instance of PolicyModel or parameters
        which accepted by this model
        """
        l_instance = instance or PolicyModel(**kwargs)
        endpoint = f"{self.engine_prefix}{self.endpoint_pattern}".format(**l_instance.dict())
        response = self.httpclient.call(
            http_good_codes=(204,), return_as=ResponseContentFormat.none,
            method='POST', endpoint=endpoint, json=l_instance.dict())

        if not response.success:
            response.message = f"Creation of the Policy {l_instance.name} FAILED: " + \
                response.message
        else:
            response.message = f"Creation of the Policy {l_instance.name} SUCCEEDED"
        logging.info(response.message)
        return response


    def delete(self, instance: PolicyModel=None, **kwargs) -> ResponseContentFormat:
        """
        Deletes the role name
        """
        l_instance = instance or PolicyModel(**kwargs)
        endpoint = f"{self.engine_prefix}{self.endpoint_pattern}".format(**l_instance.dict())
        response = self.httpclient.call(
            http_good_codes=(204,), return_as=ResponseContentFormat.none,
            method='DELETE', endpoint=endpoint, json=l_instance.dict())
        if not response.success:
            response.message = f"Deletiong of the Policy {l_instance.name} FAILED: " + \
                response.message
        else:
            response.message = f"Deletion of the Policy {l_instance.name} SUCCEEDED"
        logging.info(response.message)
        return response



class VaultProjectSecret(VaultKindBase):
    """
    Implements functions to work with secrets
    """
    def __init__(self, httpclient, engine_prefix, endpoint_pattern):
        super().__init__(httpclient, engine_prefix)
        self.endpoint = engine_prefix
        self.endpoint_pattern = endpoint_pattern


    def create(self, instance: ProjectSecretModel=None, **kwargs) -> ResponseContentFormat:
        l_instance = instance or ProjectSecretModel(**kwargs)
        endpoint = self.endpoint + '/data' + self.endpoint_pattern.format(**l_instance.dict())
        payload = dict(data=l_instance.dict()['data'])

        response = self.httpclient.call(
            http_good_codes=(200,), return_as=ResponseContentFormat.json,
            method='POST', endpoint=endpoint, json=payload)
        secret_name = self.endpoint_pattern.format(**l_instance.dict())
        if not response.success:
            response.message = f"Creation of the project secret {secret_name} FAILED: " + \
                response.message
        else:
            response.message = f"Creation of the project secret {secret_name} SUCCEEDED"
        logging.info(response.message)
        return response



    def read(self, instance: ProjectSecretModel=None, **kwargs) -> ResponseContentFormat:
        l_instance = instance or ProjectSecretModel(**kwargs)
        endpoint = self.endpoint + '/data' + self.endpoint_pattern.format(**l_instance.dict())
        payload = dict(data=l_instance.dict()['data'])

        response = self.httpclient.call(
            http_good_codes=(200,), return_as=ResponseContentFormat.json,
            method='GET', endpoint=endpoint, json=payload)
        secret_name = self.endpoint_pattern.format(**l_instance.dict())
        if not response.success:
            response.message = f"Reading of the project secret {secret_name} FAILED: " + \
                response.message
        else:
            response.message = f"Reading of the project secret {secret_name} SUCCEEDED"
            respone.data = response.data['data']
        logging.info(response.message)
        return response



    def delete(self, instance: ProjectSecretModel=None, **kwargs) -> ResponseContentFormat:
        l_instance = instance or ProjectSecretModel(**kwargs)
        endpoint = self.endpoint + '/metadata' + self.endpoint_pattern.format(**l_instance.dict())
        response = self.httpclient.call(
            http_good_codes=(204,), return_as=ResponseContentFormat.none,
            method='DELETE', endpoint=endpoint)

        secret_name = self.endpoint_pattern.format(**l_instance.dict())
        if not response.success:
            response.message = f"Deletion of the project secret {secret_name} FAILED: " + \
                response.message
        else:
            response.message = f"Deletion of the project secret {secret_name} SUCCEEDED"
        logging.info(response.message)
        return response



class VaultAppRole(VaultKindBase):
    """
    Implements functions to work with Vault AppRoles.
    The interface functions always return an instance of ResponseContentFormat model
    """
    def __init__(self, httpclient, engine_prefix, endpoint_pattern):
        super().__init__(httpclient, engine_prefix)
        self.endpoint = engine_prefix
        self.endpoint_pattern = endpoint_pattern


    def create(self, instance: CreationAppRoleModel=None, **kwargs):
        """
        Creates a new role. No content returns
        """
        l_instance = instance or CreationAppRoleModel(**kwargs)
        endpoint = self.endpoint + self.endpoint_pattern.format(**l_instance.dict())
        response = self.httpclient.call(
            http_good_codes=(204,), return_as=ResponseContentFormat.none,
            method='POST', endpoint=endpoint, json=l_instance.dict())
        if not response.success:
            response.message = f"Creation of the role {l_instance.role_name} FAILED. " + \
                response.message
        else:
            response.message = f"Creation of the role {l_instance.role_name} SUCCEEDED"
        logging.info(response.message)
        return response


    def delete(self, instance: CreationAppRoleModel=None, **kwargs):
        """
        Deletes the given role
        """
        l_instance = instance or CreationAppRoleModel(**kwargs)
        endpoint = self.endpoint + self.endpoint_pattern.format(**l_instance.dict())
        response = self.httpclient.call(
            http_good_codes=(204,), return_as=ResponseContentFormat.none,
            method='DELETE', endpoint=endpoint, json=l_instance.dict())
        if not response.success:
            response.message = f"Deletion of the role {l_instance.role_name} FAILED. " + \
                response.message
        else:
            response.message = f"Deletion of the role {l_instance.role_name} SUCCEEDED"
        logging.info(response.message)
        return response


    def role_id(self, role_name):
        """
        It returns the role's role-id
        """
        endpoint = self.endpoint + \
                   self.endpoint_pattern.format(role_name=role_name) + \
                   '/role-id'
        response = self.httpclient.call(
            http_good_codes=(200,), return_as=ResponseContentFormat.json,
            method='GET', endpoint=endpoint, json={'role_name': role_name})
        if not response.success:
            response.message = f"Getting the role {role_name}'s role-id FAILED. " + \
                response.message
        else:
            response.content = response.content['data']['role_id']
            response.message = f"Getting the role {role_name}'s role-id SUCCEEDED"
        logging.info(response.message)
        return response


    def secret_id(self, role_name):
        """
        It returns a secret-id for the given role_name
        """
        endpoint = self.endpoint + \
                   self.endpoint_pattern.format(role_name=role_name) + \
                   '/secret-id'
        response = self.httpclient.call(
            http_good_codes=(200,), return_as=ResponseContentFormat.json,
            method='POST', endpoint=endpoint, json={})
        if not response.success:
            response.message = f"Getting the role {role_name}'s secret-id FAILED. " + \
                response.message
        else:
            response.message = f"Getting the role {role_name}'s secret-id SUCCEEDED"
            response.content = response.content['data']['secret_id']
        logging.info(response.message)
        return response


    def login(self, role_id, secret_id):
        """
        It returns a token with policies assigned to the role
        provided as role_id and secret_id
        """
        endpoint = f"{self.endpoint}/login"
        response = self.httpclient.call(
            http_good_codes=(200,), return_as=ResponseContentFormat.json,
            method='POST', endpoint=endpoint,
            json=dict(role_id=role_id, secret_id=secret_id))
        if not response.success:
            response.message = f"AppRole authentication by the role-id {role_id} FAILED. " + \
                response.message
        else:
            role_name = response.content['auth']['metadata']['role_name']
            response.message = f"AppRole authentication of the role {role_name} SUCCEEDED"
            response.content = response.content['auth']['client_token']
        logging.info(response.message)
        return response

