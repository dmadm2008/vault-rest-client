#
# -*- coding: utf-8
#

from .vault import (
    VaultAppRole,
    VaultProjectSecret,
    VaultPolicy,
    VaultInit,
    create_project_namespace,
    delete_project_namespace
)
from services.generic.core import ServiceBase, HttpClient
from services.vault.models import ProjectSecretModel
from .models import ServiceConfigModel





class VaultService(ServiceBase):
    """
    Implements the public API for using it as a module.
    It wraps up the VaultSecret, VaultAppRole, VaultPolicy, etc into
    a single public interface.
    """
    def __init__(self, config, authtoken):
        super().__init__(config=config, model=ServiceConfigModel)
        self.headers.update({'X-Vault-Token': authtoken})
        self.project_approle = VaultAppRole(self,
            self.config.namespaces.project_approles_prefix,
            self.config.namespaces.project_approles_pattern)
        self.project_secret = VaultProjectSecret(self,
            self.config.namespaces.project_secrets_prefix,
            self.config.namespaces.project_secrets_pattern)
        self.policy = VaultPolicy(self,
            self.config.namespaces.policy_prefix,
            self.config.namespaces.policy_pattern)


    def call(self, http_good_codes, return_as, **kwargs):
        return super().call(http_good_codes, return_as, post_call=True, **kwargs)


    def post_call(self, call_result, http_good_codes, return_as, **kwargs):
        if not call_result.success and call_result.http_code == 503: # sealed
            # trying to unseal it,
            # if it was success then we will re-run the initial failed
            # request
            if self.unseal():
                return self.call(http_good_codes, return_as, **kwargs)
        return call_result


    def unseal(self):
        """
        Performs checking and unsealing if needed.
        Usually execute this upon initializing of the instance
        """
        vault = VaultInit(self)
        if not vault.is_sealed():
            response = vault.unseal(self.config.endpoint.unseal_keys)


    def create_project_namespace(self, template_filepath, params):
        """
        Provisions the resources into vault store from specified in the template
        """
        return create_project_namespace(template_filepath, params,
            controllers=dict(
                project_secrets=self.project_secret,
                project_approles=self.project_approle,
                policies=self.policy
            ))


    def delete_project_namespace(self, template_filepath, params):
        """
        It delete the resources from vault store from specified in the template
        """
        return delete_project_namespace(template_filepath, params,
            controllers=dict(
                project_secrets=self.project_secret,
                project_approles=self.project_approle,
                policies=self.policy
            ))



