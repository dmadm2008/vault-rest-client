#
# -*- coding: utf-8
#

from pydantic import BaseModel
from services.generic.models import HttpClientConfigModel



class EndpointModel(HttpClientConfigModel):
    token: str = ""



class FilenamePatternsModel(BaseModel):
    container_pkcs12: str
    container_pkcs12_passcode: str



class PKIProxyConfigModel(BaseModel):
    endpoint: EndpointModel
    filename_patterns: FilenamePatternsModel


