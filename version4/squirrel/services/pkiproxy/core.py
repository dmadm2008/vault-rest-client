#
# -*- coding: utf-8
#

from .models import PKIProxyConfigModel
from services.generic.core import ServiceBase
from services.generic.models import HttpClientResponseModel



class PKIProxy(ServiceBase):
    def __init__(self, config):
        super().__init__(config=config, model=PKIProxyConfigModel)


    def get_pkcs12(self):
        pass


    def get_passcode(self):
        pass


