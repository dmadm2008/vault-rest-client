#
# -*- coding: utf-8
#
from pydantic import BaseModel, validator, HttpUrl
from enum import Enum, IntEnum
from typing import Any



class HttpClientResponseModel(BaseModel):
    success: bool
    http_code: int
    content: Any = None
    message: str = ""



class ResponseContentFormat(IntEnum):
    none = 0
    json = 1
    text = 2
    raw = 3



class HttpClientConfigModel(BaseModel):
    url: HttpUrl
    @validator('url')
    def normalize_url(cls, v):
        while v.endswith('/'):
            v = v[:-1]
        return v


class ServiceBaseModel(BaseModel):
    pass




