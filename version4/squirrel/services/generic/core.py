#
# -*- coding: utf-8
#

import logging
import requests
from .models import (
    HttpClientResponseModel,
    ResponseContentFormat,
    HttpClientConfigModel
)



def m(**kwargs):
    return kwargs['msg'].format(**kwargs)



class HttpClient(requests.Session):
    def __init__(self, url):
        super().__init__()
        self.model = HttpClientConfigModel(url=url)
        self.headers.update({'Content-Type': 'application/json'})


    def pre_call(self, **kwargs):
        """
        It's being called once the call method is invoked.
        It should return an instance of HttpClientResponseModel with
        success=true to proceed with the call.
        """
        return HttpClientResponseModel(True, 0, None, "")


    def post_call(self, call_result, http_codes, return_as, **kwargs):
        """
        It's called once the call() is completed.
        call_result is the instance which returned by the call() method
        http_codes, return_as, **kwargs are the parameters passed
        to the call()
        """
        return call_result


    def call(self, http_good_codes, return_as: ResponseContentFormat, **kwargs):
        """
        There are two special parameters might be provided:
        - pre_call(False) and post_call(False) which instruct the method
        to call the methods pre_call() and post_call() accordingly.
        The default values is False for both triggers so it should be set to
        true if you need the triggers to execute.
        The method returns an instance of HttpClientResponseModel().
        """
        post_call = kwargs.pop('post_call', False)
        pre_call = kwargs.pop('pre_call', False)

        if pre_call:
            precall = self.pre_call(http_good_codes, return_as, **kwargs)
            if not precall.success:
                return precall
        url = self.model.url + kwargs.pop('endpoint')
        status_code, status_msg, message, response = -1, "UNDEF", "", None
        try:
            response = self.request(url=url, **kwargs)
            status_code = response.status_code
            status_msg = "SUCCESS"
        except requests.exceptions.ConnectionError as ce:
            message = str(ce)
            logging.error(m(msg="Connection error occured: {error}", error=ce))
            status_msg = "FAIL"
        data = None
        if response is not None:
            if status_code in http_good_codes:
                if return_as == ResponseContentFormat.json:
                    data = response.json()
                elif return_as == ResponseContentFormat.text:
                    data = response.text
                elif return_as == ResponseContentFormat.raw:
                    data = response.content
                elif return_as == ResponseContentFormat.none:
                    data = ''
                else:
                    raise NotImplementedError("Requested response format is not supported")
            message = response.text.rstrip()
        logging.debug(m(msg="Request ({method}){url} completed with {status}",
                         method=kwargs['method'].upper(), url=url,status=status_msg))
        result = HttpClientResponseModel(
            success=(status_code in http_good_codes),
            http_code=status_code,
            content=data, message=message)
        if post_call:
            return self.post_call(result, http_good_codes, return_as, **kwargs)
        return result



class ServiceBase(HttpClient):
    def __init__(self, config, model, httpclient_class=HttpClient):
        self.json_config = config
        self.config = model(**config)
        super().__init__(self.config.endpoint.url)

