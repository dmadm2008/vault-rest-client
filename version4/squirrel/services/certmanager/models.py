#
# -*- coding: utf-8
#

from pydantic import BaseModel
from typing import List, Dict, Any



class CertificateKinds(Enum):
    ca: str = "ca"
    crt: str = "crt"
    key: str = "key"
    pkcs12: str = "pkcs12"
    jks: str = "jks"



class CertificateDeliveryFormat(Enum):
    native: str = "native"
    base64: str = "base64"
    json: str = "json"
    passcode: str = "passcode"
    asfile: str = "asfile"



class CertificateReadModel(BaseModel):
    fqdn: str
    kind: CertificateKind
    output: CertificateDeliveryFormat



