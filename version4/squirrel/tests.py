#!/usr/bin/env
#
# -*- coding: utf-8
#


import unittest
from services.vault.core import VaultService


vs_config = dict(
    endpoint = dict(url = 'http://127.0.0.1:8200',
        token = 's.OGnIJi5U4rNGuwev0XAPeGim',
        unseal_keys = [
            "CGaz0tHbr9udyGR0RW2ZepzLIG5KvFqfNARXcsK26rE3",
            "mXX71o38eytL3gPgoNR0sDqF4e4Yz3ub6ml/tz3hFQwa",
            "VTSx7aj2EuK1J52YEEut/rwiH3oZsoO1+IPIKimG/zK9",
            "nnmz3aoDrvvMcjnRRshpRAdks9oSzsCgt+UIuLmU89dW",
            "3iHXdtsD7BY7F9DnUU3I7iRm3swBGF0Ta437VclYsp9Y"
        ]),
    api_endpoints = dict(
        ping = '/ping',
        install = '/installCert',
        detail = '/getDetails'
    ),
    namespaces = dict(
        project_approles_prefix = '/v1/auth/project-approles',
        project_approles_pattern = '/role/{role_name}',
        project_secrets_prefix = '/projects',
        project_secrets_pattern = '/{project}/{environ}/{component}/{version}/{category}/{alias}',
        policy_prefix = '/v1/sys/policy',
        policy_pattern = '/{name}'
    ),
    project = dict(categories = ["accounts", "envvars", "settings"])
)


vault = VaultService(vs_config, vs_config['endpoint']['token'])


class TestVaultAppRole(unittest.TestCase):
    def test_projectrole(self):
        approle_ex = dict(role_name='autotestrole', policies=['default','myproject-dev-web-reader'])
        role_name = approle_ex['role_name']
        self.assertTrue(vault.project_approle.create(**approle_ex)), "Create role failed"
        req_roleid = vault.project_approle.role_id(role_name)
        self.assertTrue(req_roleid.success), "Getting role-id failed"
        req_secretid = vault.project_approle.secret_id(role_name)
        self.assertTrue(req_secretid.success), "Getting secret-id failed"
        self.assertTrue(vault.project_approle.login(req_roleid.content, req_secretid.content))
        self.assertTrue(vault.project_approle.delete(role_name=role_name)), "Delete role failed"

        #self.assertTrue(vault.project_approle.create(**approle_ex)), "Create role failed"
        #self.assertTrue(vault.project_approle.delete(**approle_ex)), "Delete role failed"


class TestVaultPolicy(unittest.TestCase):
    def test_policy(self):
        policy = dict(name="autotestpolicy", policy="""
path "/certificates/*" {
    capabilities = [ "create", "update", "list", "delete" ]
}
        """)
        self.assertTrue(vault.policy.create(**policy).success), "Creation of policy failed"
        self.assertTrue(vault.policy.delete(**policy).success), "Deletion of policy failed"



class TestVaultProjectSecret(unittest.TestCase):
    def test_projectsecret(self):
        secret = dict(
            project = "myproject",
            environ = "dev",
            component = "apicore",
            version = "beta1",
            category = "envvars",
            alias = "default",
            data = dict(
                MONGODB_CONN = "mongodb://127.0.0.1:27017",
                MONGODB_USER = "mongouser",
                MONGODB_PASS = "mongopass"
            ),
            metadata = dict()
        )
        #self.assertTrue(vault.project_secret.create(**secret).success), \
        #    "Creation of the project secret failed"
        #self.assertTrue(vault.project_secret.delete(**secret).success), \
        #    "Deletion of the project secret failed"


class TestVaultProject(unittest.TestCase):
    def test_projectcreate(self):
        project = dict(project = "myproject", environ = "dev", component = "web",
                       version = "beta1")
        vault.create_project_namespace("project-template.yaml", project)
        vault.delete_project_namespace("project-template.yaml", project)


if __name__ == "__main__":
    unittest.main()

