#!/bin/bash

filename="$1"

openssl pkcs12 -in "$filename" -passin "file:${filename}.pwd" -nodes -nokeys 2>/dev/null | \
    openssl x509 -noout -text | awk '/^\s*DNS:/{print}'

