#!/usr/bin/env python
#
# -*- coding: utf-8
#
#
# Tinyproxy.
#
# == How it works.
# There are two endpoints available for use.
# === GET /request/<filename>
# This request should also supply the header HTTP-DNS with fqnd(s) separated by a comma.
# <filename> should be a name of a file to lookup at the local file system in CERT_DIR
# Upon the request the service will do:
# - reading the requested file (if found)
# - getting alt DNS names which is embeded in the certificate
# - caching the certificate in the memory
# - if the requested in HTTP-DNS fqdns matches the found certificate
#   it will return a JSON with the certificate, passcode and some other information
# Upon next request to the same filename/fqdn(s) will return the certificate from the cache.
#
#
# === POST /flush
# It allows to remove an entry from the cache
# Provide a header HTTP-DNS which represents a list of fqdn-es separated by a comma
#
#
#
import os
from io import BytesIO, StringIO
import base64
from time import time, sleep

from bottle import Bottle, request, abort, response
from subprocess import check_output
import logging


GETDNS_SCRIPT = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'certaltdns.sh')
CERT_DIR = os.path.dirname(os.path.realpath(__file__))
CERT_FILENAME = 'cert.p12'

app = Bottle()
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)



@app.post("/request/<filename>")
def get_content(filename):
    dnses = request.headers.get('HTTP-DNS')
    if dnses is not None:
        return proxy.get_cert(dnses.split(','), filename=filename)
    response.status = 400
    return "The request should supply the header HTTP-DNS"



@app.post('/flush')
def reset_cache():
    dnses = request.headers.get('HTTP-DNS')
    l_dnses = dnses.split(',') if dnses is not None else None
    return proxy.reset_cache(l_dnses)



def Result(success, message, content=None, code=None):
    return dict(success=success, code=code, content=content, message=message)



class ProxyCertCache(object):
    """
    Provides the interface to the certificates.
    Caching certificates in memory
    """
    def __init__(self, certdir, certfile):
        self.local_dir = certdir
        self.local_file = certfile
        self.memcache = []
        self.last_cache_check = 0
        self.max_cache_entries = 10


    def refresh_cache(self, filename=None):
        """
        Reads the local file in the cache
        """
        # don't allow to run the function in less than 3 seconds
        curr_cache_check = time()
        if (curr_cache_check - self.last_cache_check) > 2.5:
            self.last_cache_check = curr_cache_check
        else:
            return
        l_filename = self.local_file if filename is None else filename
        l_response = get_dns(os.path.join(CERT_DIR, l_filename))
        if not l_response['success']:
            return
        content = l_response['content']
        d_fqdns, pkcs12 = content['fqdns'], content['pkcs12']
        if len(self.memcache) == 0:
            logging.info("Added to cache {}".format(d_fqdns))
            self.memcache.append(content)
        else:
            for cache_entry in self.memcache:
                c_fqdns = cache_entry['fqdns']
                if not is_equal(d_fqdns, c_fqdns):
                    self.memcache.append(content)
                    logging.info("Added to cache {}".format(d_fqdns))
                    if len(self.memcache) > self.max_cache_entries:
                        old = self.memcache.pop()
                        logging.info("Removed from cache {}".format(old['fqnds']))


    def get_cert(self, dns_aliases, filename=None):
        """
        Looks up the fqdn in the cache. It is not found in it
        it lookups the certificate from a file system,
        doing caching as well
        """
        self.refresh_cache(filename)
        content = None
        for entry in self.memcache:
            fqdns = entry['fqdns']
            if is_equal(dns_aliases, fqdns):
                content = entry
                break
        else:
            response.status = 404
            return "Requested content not found: {}".format(dns_aliases)
        return content


    def reset_cache(self, fqdns=None):
        """
        Cleans the cache. If fqdn(s) is provided
        it cleans the only this certificate
        """
        if fqdns is None:
            logging.info("Removed {} entries from cache".format(len(self.memcache)))
            self.memcache = []
            return "ok"
        del_items = 0
        for index, entry in enumerate(self.memcache):
            c_fqdns = entry['fqdns']
            if is_equal(fqdns, c_fqdns):
                del self.memcache[index]
                del_items += 1
        logging.info("Removed {} entry(-es) from cache".format(del_items))
        return "ok"



def get_dns(filename):
    """
    Returns a list of alternative DNS names configured in the PKCS12
    certificate
    """
    if not os.path.exists(filename):
        return Result(False, message="Requested file {} not found".format(filename))
    if os.path.getsize(filename) < 4:
        return Result(False, message="File {} found but it's size is zero".format(filename))
    if 1:
        fqdns = []
        output = check_output([GETDNS_SCRIPT, filename]).decode()
        # it processes the lines in format
        # DNS:fqdn1, DNS:fqdn2, DNS:fqdn3, ...
        # and returns a list of fqdn's with the base64 encoded pkcs12 file
        # and its password.
        # It returns a dictionary with the keys: fqdns, content, passcode.
        #, pkcs12 and passcode a base64 encoded, fqdns contains a list of
        # fqdns mateches to the pkcs12
        for t_line in output.split('\n'):
            records = [item.strip() for item in t_line.split(',')]
            for item in records:
                if item.startswith('DNS:'):
                    fqdns.append(item[4:])


        if len(fqdns) > 0:
            pkcs12_data, passcode_data = None, None
            with open(filename, 'rb') as pkcs12_fd:
                pkcs12_data = base64.encodestring(pkcs12_fd.read())
            with open(filename + '.pwd', 'rb') as passcode_fd:
                passcode_data = base64.encodestring(passcode_fd.read().encode())
            content = dict(fqdns=fqdns, pkcs12=pkcs12_data.strip(), format='base64',
                           passcode=passcode_data.strip())
            try:
                # since we don't have permissions to delete files
                # we truncate its size to 0
                with open(filename, 'rw') as fd:
                    fd.truncate(0)
            except:
                pass
            return Result(True, content=content,
                    message="Found {} FQDN/AltDNS matches".format(len(fqdns)))
        return Result(False, message="No FQDN/AltDNS matches found")



def is_equal(fqdn_set1, fqdn_set2):
    """
    It returns true if items from the fqdn_set1 are in the fqdn_set2
    otherwise it's false.
    """
    return (set(fqdn_set1) - set(fqdn_set2)) == set()


proxy = ProxyCertCache(CERT_DIR, CERT_FILENAME)



if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True, reload=True)

