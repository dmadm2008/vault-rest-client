#!/usr/bin/env python
#
# -*- coding: utf-8
#

import logging

class VaultEngine(object):
    def __init__(self, http_client, authtoken, endpoint_prefix):
        self.http_client = http_client
        self.endpoint_prefix = endpoint_prefix
        self.authtoken = authtoken

    def apicall(self, method, endpoint, payload, headers, good_http_codes, return_datatype):
        endpoint = self.endpoint_prefix + endpoint
        l_headers = {'X-Vault-Token': self.authtoken}
        l_headers.update(headers)
        response = self.http_client.request(method=method, url=endpoint, headers=l_headers)
        success, data, message = False, None, ""
        if response.status_code in good_http_codes:
            success = True
            if return_datatype == str:
                data = response.text
            elif return_datatype == bytes:
                data = response.content
            else:
                raise Exception("Specified not supported '{}' data type".format(return_datatype))
        message = "Request {} at {} completed with".format(method.upper(),
            'SUCCESS' if success else 'FAILURE')
        return success, data, message


class VaultAppRole(VaultEngine):
    def __init__(self, http_client, authtoken, prefix):
        super().__init__(http_client, authtoken, prefix)

    def create(self, role_name, payload):
        return self.apicall('POST', "/role/" + role_name, good_http_codes=(200,))

    def delete(self, role_name):
        return self.apicall('DELETE', "/role/" + role_name, good_http_codes=(204,))

    def role_id(self, role_name):
        ok, data, msg = self.apicall('POST', "/role/" + role_name + "/role-id", good_http_codes=(204,))
        if ok:
            data = data['data']['role_id']
        return ok, data, msg

    def secret_id(self, role_name):
        return self.apicall('POST', "/role/" + role_name + "/secret-id", good_http_codes=(204,))

    def login(self, role_name, role_id):
        ok, data, msg = self.apicall('POST', "/role/" + role_name + "/login",
                                     payload={'role_id': role_id})


class VaultSecret(VaultEngine):
    def __init__(self, http_client, authtoken, prefix):
        super().__init__(http_client, authtoken, prefix)

    def read(self, namespace):
        pass

    def write(self, namespace, payload, metadata={}):
        pass

    def delete(self, namespace):
        pass


class VaultPolicy(VaultEngine):
    def __init__(self, http_client, authtoken, prefix):
        super().__init__(http_client, authtoken, prefix)




