#!/usr/bin/env python
#
# -*- coding: utf-8
#

import os
import subprocess
import argparse
from tempfile import NamedTemporaryFile


def getjks(pkcs12_data, inpass, outpass):
    pkcs12_file = NamedTemporaryFile(mode='w+b', delete=True)
    pkcs12_file.file.write(pkcs12_data)
    pkcs12_file.file.flush()
    jks_file = "{}_jks".format(pkcs12_file.name)

    keytool_cmd = [ 'keytool', '-importkeystore', '-srckeystore', pkcs12_file.name,
        '-srcstoretype', 'pkcs12', '-deststoretype', 'jks', '-srcstorepass', inpass,
        '-deststorepass', outpass, '-destkeystore', jks_file, '-noprompt']
    ok, data, message = False, None, ""
    try:
        p = subprocess.check_output(keytool_cmd, stderr=subprocess.STDOUT)
        ok, message = True, "Certificate jks has been received from pkcs12"
        data = open(jks_file, 'rb').read()
    except subprocess.CalledProcessError as exc:
        message = exc.output
        print("Could not convert pkcs12 certificate to jks: {}".format(exc.output))
    finally:
        try:
            os.remove(jks_file)
        except Exception as exc:
            print("Error while deleting a temporary jks file: {}".format(exc))
    return ok, data, message


def main_cli():
    parser = argparse.ArgumentParser()
    parser.add_argument('--pkcs12', required=True, type=argparse.FileType('r'), help='Source pkcs12 file')
    parser.add_argument('--jks', required=True, type=argparse.FileType('w'), help='Destination jks file')
    parser.add_argument('--inpass', required=True, help='Password on pkcs12')
    parser.add_argument('--outpass', required=True, help='Password for jks')

    args = parser.parse_args()
    print(args)
    ok, data, message= getjks(open(args.pkcs12.name, 'rb').read(), 'test', 'pass123')
    if ok:
        open(args.jks.name, 'wb').write(data)

    # open(args.jks.name, 'wb').write(jks_data)

if __name__ == "__main__":
    main_cli()

