#!/usr/bin/env python

import os
import sys
import copy
import logging
import argparse
import requests
from json import loads as json_loads
from yaml import load as yaml_loadf

#try:
#    _ = unicode
#except:
#    unicode = str

def _(**kwargs):
    print(" >>> {0}".format(kwargs))
    pass


def validate_params(required=[], optional=[], params={}):
    passed_keys = list(params.keys())
    passed_unexpected, passed_required, passed_optional = [], [], []
    for k in passed_keys:
        if (k in required) and (params[k] not in ('', None)):
            passed_required.append(k)
        elif (k in optional):
            passed_optiona.append(k)
        else:
            passed_unexpected.append(k)
    assert(len(required) == len(passed_required)), \
        "Missed required params or their values: {0}".format(set(required) - set(passed_required))
    assert(passed_unexpected == []), \
        "Passed unexpected params: {0}".format(passed_unexpected)


def render_config(json_config, params):

    def render_item(value):
        if isinstance(value, (unicode, str)):
            print("Rendering: value={0}, params={1}".format(value, params))
            return value.format(**params)
        elif isinstance(value, (list, tuple)):
            for v in value:
                render_item(v)
        elif isinstance(value, dict):
            for k, v in value.items():
                render_item(v)
        else:
            raise Exception("This point should not be reached")
    return render_item(json_config)


class RESTClient(object):
    #
    # API docs: https://www.vaultproject.io/api-docs/
    #
    def __init__(self, **kwargs):
        validate_params(required=('endpoint', 'token'), params=kwargs)
        self.__params = kwargs
        self.__session = requests.Session()
        self.__session.headers = {
            'X-Vault-Token': kwargs.get('token', ''),
            'Content-Type': 'application/json'
        }

    @property
    def p(self):
        return self.__params
    pass


    def make_call(self, **kwargs):
        pass


    def login(self):
        """ checks the provided endpoint and token. Returns True if it's Ok """
        pass


def load_config(from_string, from_file):
    """
    Returns JSON config. From string takes a priority if both provided
    """
    buffer = None
    if (from_string is None) and (from_file is None):
        logging.error(_(msg="Configuration is not provided"))
        return
    elif from_string is not None:
        buffer = from_string
        logging.info(_(msg="Taking JSON configuration from a command line"))
    elif from_file is not None:
        if os.path.exists(from_file):
            try:
                buffer = open(from_file, 'r').read()
                logging.info(_(msg="Taking JSON configuration from a file: {file}", file=from_file))
            except Exception as exc:
                logging.error(_(msg="Cannot read JSON configuration from file: {file}. Error: {error}", file=from_file, error=exc))
        else:
            logging.error(_(msg="Configuration file does not exist: {file}", file=from_file))
    if buffer is not None:
        try:
            return json_loads(buffer)
        except Exception as exc:
            logging.error(_(msg="Given configuration is not a valid JSON object: {error}", error=exc))


def get_onestep_params(user_config):
    indexes = {k: 0 for k in user_config.keys()}

    if True:
        for key, values in user_config.items():
            for v_id, value in enumerate(values):
                indexes[key] = v_id
                params = {k: idx for k, idx in indexes.items()}
                yield params



def execute_action(**kwargs):
    validate_params(required=('action', 'rest_client', 'user_config', 'project_template'), params=kwargs)
    print("Passed keys: {0}".format(kwargs.keys()))

    def perform_action(kind, items, user_config):
        """ kind is one of: secrets, policies, groups """
        print(
            ">>> Kind: {kind}\n"
            ">>> Items: {items}\n"
            ">>> User config: {user_config}".format(kind=kind, items=items, user_config=user_config)
        )
        result_list = []
        for scope in user_config.get('scopes', []):
            for component in user_config.get('components', []):
                for item in items:
                    print("Item({1}): {0}".format(item, kind))
                    result_list.append(render_config(item, params={
                        'project': user_config['project'],
                        'component': component,
                        'scope': scope
                    }))
        return result_list

    kinds = {'secrets': [], 'policies': [], 'groups': []}
    user_config = kwargs.get('user_config', {})
    for kind in kinds.keys():
        kinds[kind] = perform_action(kind=kind,
            items=kwargs['project_template'].get(kind, []), user_config=kwargs['user_config'])

    return kinds

def add_config(**kwargs):
    execute_action(action='add', **kwargs)

def remove_config(**kwargs):
    pass

def main_cli():
    actions = ('add', 'remove')
    parser = argparse.ArgumentParser(description='Hashicorp Vault client')
    parser.add_argument('-e', '--endpoint', default='https://127.0.0.1:8200', help='Endpoint of vault server')
    parser.add_argument('-t', '--token', help='Authorization token')
    parser.add_argument('-a', '--action', required=True, choices=actions, help='Select action to perform')
    parser.add_argument('--json-config', help='JSON config as string')
    parser.add_argument('--json-file', help='JSON config from a file')
    parser.add_argument('--project-template', default='project-template.yaml', help='Template file to proceed')
    args = parser.parse_args()

    json_config = load_config(args.json_config, args.json_file)


    for id, param in enumerate(get_onestep_params(json_config), start=1):
        print("{0:2}. {1}".format(id, param))

    return


    req_params = ('token', 'endpoint')
    rs = RESTClient(**{p: v for p, v in args.__dict__.items() if p in req_params})

    if args.action in ('add', 'remove'):
        if args.project_template is None:
            parser.error("Parameter --project-template must be provided")
        elif not os.path.exists(args.project_template):
            parser.error("Template file (--project-template) '{0}' must exist".format(args.project_template))
        template = None
        try:
            template = yaml_loadf(open(args.project_template, 'r'))
        except Exception as exc:
            parser.error("Cannot load template file (--project-template): {0}".format(exc))
        {
            'add': add_config,
            'remove': remove_config
        }[args.action](rest_client=rs, user_config=json_config, project_template=template)

if __name__ == "__main__":
    main_cli()
